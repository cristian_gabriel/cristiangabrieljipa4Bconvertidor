package com.example.cristian97.ta1cristianjipa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button  siguiente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("MainActivity","Cristian Gabriel Jipa Alvarado");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       siguiente = (Button)findViewById(R.id.btnConvertidor);

       siguiente.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent cambiar = new Intent(MainActivity.this,Convertidor.class);
               startActivity(cambiar);
           }
       });

    }
}
