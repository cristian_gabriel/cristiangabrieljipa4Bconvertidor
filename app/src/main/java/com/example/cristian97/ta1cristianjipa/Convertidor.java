package com.example.cristian97.ta1cristianjipa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Convertidor extends AppCompatActivity {
Button  buttonConvertir;

EditText temperaturac, temperaturaf;
double f= 1.8;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor);
        temperaturac = (EditText)findViewById(R.id.txtC);
        temperaturaf = (EditText)findViewById(R.id.txtF);

        buttonConvertir =(Button)findViewById(R.id.btnConvertir);


        buttonConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double temp = Double.parseDouble(temperaturac.getText().toString());
                double tempfn = temp * f + 32;
                temperaturaf.setText(""+ tempfn+ "F");
            }
        });



    }
}
